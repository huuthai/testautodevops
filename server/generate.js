var faker = require('faker');

var database = { products: []};

for (var i = 0; i <= 300; i++) {
  database.products.push({
    id: 1,
    name: faker.commerce.productName(),
    description: faker.lorem.sentences(),
    price: faker.commerce.price(),
    imageUrl: "https://unsplash.com/photos/Wzn5IgxXWKo",
    quantity: faker.random.number()
  });
}

console.log(JSON.stringify(database));